import time
from bluesky.plan_stubs import mv, abs_set, read
from bluesky.plans import count,scan, list_grid_scan, grid_scan
from bluesky import Msg
from bluesky.plan_stubs import null
from IPython import get_ipython

user_ns = get_ipython().user_ns


#######################
# Define general functions

# Define functions that control the output  
def get_pos(dict_device_):
    width = 0
    precision = 4
    list_readback = []
    list_device = []
    list_components = []
    device = list(dict_device_.keys())
    components = list(dict_device_[device[0]].keys())
    #value_of_component = list(dict_in[device[0]].values())
    for j in device:
        for i in components:        
            create_string_for_getattr_readback = f'{i}.readback'
            readback_get_full = getattr(user_ns[j], create_string_for_getattr_readback)
            readback_get_value = readback_get_full.get()
            list_readback.append(readback_get_value)
            list_device.append(j) 
            list_components.append(i)  

    
    tuple_final_values = list(zip(list_device, list_components, list_readback))
    for entries in tuple_final_values:
            if type(readback_get_value) != str:
                print(f'Current position of {entries[0]}.{entries[1]}: {round(entries[2], 2):{width}.{precision}}')
                #print(f'Current position of {entries[0]}.{entries[1]}: {round(entries[2], 2)}')
            else:
                print(f'Current position of {entries[0]}.{entries[1]}: {(entries[2])}')
 
 
def get_pos_global_dict(dict_device_, device_value):
    width = 0
    precision = 4
    list_readback = []
    list_device = []
    list_components = []
    all_devices = list(dict_device_.keys())
    device = all_devices[device_value]
    components = list(dict_device_[device].keys())
    #value_of_component = list(dict_in[device[0]].values())
    for j in device:
        for i in components:        
            create_string_for_getattr_readback = f'{i}.readback'
            readback_get_full = getattr(user_ns[j], create_string_for_getattr_readback)
            readback_get_value = readback_get_full.get()
            list_readback.append(readback_get_value)
            list_device.append(j) 
            list_components.append(i)  

    
    tuple_final_values = list(zip(list_device, list_components, list_readback))
    for entries in tuple_final_values:
            if type(readback_get_value) != str:
                print(f'Current position of {entries[0]}.{entries[1]}: {round(entries[2], 2):{width}.{precision}}')
                #print(f'Current position of {entries[0]}.{entries[1]}: {round(entries[2], 2)}')
            else:
                print(f'Current position of {entries[0]}.{entries[1]}: {(entries[2])}')


#######################        
# create dict that contains all devices  
    
dict_devices = {'u49_2_ph' : {'h' : '', 
                              'v' : ''},
                'u49_2_au' : {'top' : '', 
                            'bottom' : '',
                            'right' : '',
                            'left' : ''},
                'u49_2_smu_pgm2' : {'t_x' : '',
                           'r_x' : '',
                           'r_y' : '',
                           'r_z' : ''},                
                } 
 


###########################################
# Auxiliary functions already implemented #
###########################################

############################################################################
# UNDULATOR

############################################################################
# PINHOLE

def read_pinhole():
    #print(f'Current position of {}: {user_ns["u49_2_ph"].h.readback.get()}')
    get_pos_global_dict(dict_devices, 0)
    return(yield from null())

"""
def read_pinhole_b():
    #print("\nI move frontend pinholes to home position ")
    #yield Msg('open_run')
    #yield from read(user_ns["u49_2_ph"].h.readback)
    print(f'{user_ns["u49_2_ph"].h.readback.get()}')
    yield Msg('open_run')
    yield from read(user_ns["u49_2_ph"].h.readback)
    yield Msg('close_run')
        #user_ns["u49_2_ph"].v)
"""


############################################################################
# APERTURES

def read_apertures():
    get_pos_global_dict(dict_devices, 1)
    return(yield from null())


############################################################################
# SMU PGM2

def read_smu_pgm2():
    get_pos_global_dict(dict_devices, 3)
    return(yield from null())


############################################################################
# SLITS
