
import time

from bluesky.plan_stubs import mv, abs_set
from bluesky.plans import count,scan, list_grid_scan, grid_scan
from bluesky.plan_stubs import null

from IPython import get_ipython
user_ns = get_ipython().user_ns

#######################
# how to implement correctly composed functions
'''
def test_function():
    yield from test1()
    test2()
def test1():
    yield from scan([user_ns["det"]], user_ns["motor"], 5, 10, 10)
def test2():
    print('yo', 3)
    print(user_ns["u17_valve"+str(4)+"_cat"].readback.get())
'''
##################
# switching beamline functions
"""
def SISSY_I_PGM():
    fset_closeshutter()   
    yield from fset_U17_PGM()         
    yield from fset_pinhole()
    yield from fset_AU1_middle()
    fset_M1()   # change back to yield from when problem is solved

    print("\nExpected intensities:   -small AU1  (0.2 x 0.4): kth12/25/26 (diode(mesh)) 18 uA /  5 nA /  4 uA /")
    print("\n (@867.25 eV, 7 mm gap) -middle     (0.5 x 1.0):                          120 uA / 37 nA / 27 uA (0.8 nA)/")
    print("\n                        -large      (1.0 x 2.0):                          340 uA / 90 nA / 70 uA /")
"""

###########################################
# Auxiliary functions already implemented #
###########################################

############################################################################
# UNDULATOR
# define parameters 
user_ns_undulator_name = 'u49_2'


"""
def fset_U17_PGM():
    print("\nI move U17-gap to 7.0 mm (corresponding to 867.25 eV (Ne 1s) in 1st harmonic) ")
    yield from mv(
        user_ns["u17_pgm"].ID_on, 0,
        user_ns["u17"].gap, 7.0)

def fset_U17_DCM():
    print("\nI move U17-gap 7.629 mm (corresponding to 5 keV in 5th Harmonic)")
    yield from mv(
        user_ns["u17_dcm"].ID_on, 0,
        user_ns["u17"].gap, 7.629)



def fset_U17_home():
    print("\nI move U17-gap to a safe position of 10 mm ")
    yield from mv(
        user_ns["u17_dcm"].ID_on, 0,
        user_ns["u17"].gap, 10.0)
"""
############################################################################
# PINHOLE

# define parameters 
user_ns_pinhole_name = 'ph'
# limits for user input
h_low_limit = 0
h_high_limit = 0
v_low_limit = 0
v_high_limit = 0


def move_pinhole_home():
    #print("\nI move frontend pinholes to home position ")
    yield from mv(
        user_ns[user_ns_pinhole_name].h, 0,
        user_ns[user_ns_pinhole_name].v, 0)


## input by user 

def move_pinhole_user(_h, _v):
    # if set values are outside of motor position range stop/do NOT the move! 
    if (h_low_limit <= _h <= h_high_limit) and (v_low_limit <= _v <= v_high_limit):
        yield from mv(
            user_ns[user_ns_pinhole_name].h, _h,
            user_ns[user_ns_pinhole_name].v, _v)
    elif (h_low_limit <= _h <= h_high_limit) == False:
        print('The used parameter for the horizontal movement is out of range! Limits are: {h_low_limit} to {h_high_limit}.') 
        return(yield from null())
        #return null and message 
    elif (v_low_limit <= _v <= v_high_limit):
        print('The used parameter for the vertical movement is out of range! Limits are: {v_low_limit} to {v_high_limit}.') 
        return(yield from null())
    
    
############################################################################
# APERTURES

# define parameters 
user_ns_au1_name = 'u49_2_au'

# limits for user input
top_low_limit = 0
top_high_limit = 0
bottom_low_limit = 0
bottom_high_limit = 0
left_low_limit = 0
left_high_limit = 0
right_low_limit = 0
right_high_limit = 0



def move_AU1():
    #print("\nI move frontend apertures AU1 to 0.4 x 0.2 mm (h x v) opening ")
    yield from mv(user_ns[user_ns_au1_name].top, 0,
          user_ns[user_ns_au1_name].bottom, 0,
          user_ns[user_ns_au1_name].right, 0,
          user_ns[user_ns_au1_name].left, 0)

## input by user 
 
def move_AU1_user(_t, _b, _l, _r):
    if (top_low_limit <= _t <= top_high_limit) and (bottom_low_limit <= _b <= bottom_high_limit) \
       (left_low_limit <= _l <= left_high_limit) and (right_low_limit <= _r <= right_high_limit):
        yield from mv(user_ns[user_ns_au1_name].top, _t,
            user_ns[user_ns_au1_name].bottom, _b,
            user_ns[user_ns_au1_name].right, _l,
            user_ns[user_ns_au1_name].left, _r)
    elif (top_low_limit <= _t <= top_high_limit) == False:
        print('The used parameter for the top movement is out of range! Limits are: {top_low_limit} to {top_high_limit}.') 
        return(yield from null())
        #return null and message 
    elif (bottom_low_limit <= _b <= bottom_high_limit) == False:
        print('The used parameter for the bottom movement is out of range! Limits are: {bottom_low_limit} to {bottom_high_limit}.') 
        return(yield from null())
    elif (left_low_limit <= _l <= left_high_limit) == False:
        print('The used parameter for the left movement is out of range! Limits are: {left_low_limit} to {left_high_limit}.') 
        return(yield from null())
    elif (right_low_limit <= _r <= right_high_limit) == False:
        print('The used parameter for the right movement is out of range! Limits are: {right_low_limit} to {right_high_limit}.') 
        return(yield from null())
    
# implement "move l, r with the same value". the distance between both is not altered     
# implement "move t, b with the same value". the distance between both is not altered  

############################################################################
# M1 MIRROR
"""
def move_MU_PGM2():
    #print("\nI set mirror M1 ")
    yield from mv(user_ns["u17_m1"].t_x, 0) 
    yield from mv(user_ns["u17_m1"].r_x, 0)
    yield from mv(user_ns["u17_m1"].r_y, 0)
    yield from mv(user_ns["u17_m1"].r_z, 0)

def move_MU_PGM2_user(_tx, _rx, _ry, _rz):
    #print("\nI set mirror M1 ")
    yield from mv(user_ns["u17_m1"].t_x, _tx) 
    yield from mv(user_ns["u17_m1"].r_x, _rx)
    yield from mv(user_ns["u17_m1"].r_y, _ry)
    yield from mv(user_ns["u17_m1"].r_z, _rz)
"""

############################################################################
# SMU 

# define parameters 
user_ns_smu_name = 'u49_2_smu_pgm2'

# limits for user input
t_x_low_limit = 0
t_x_high_limit = 0
r_x_low_limit = 0
r_x_high_limit = 0
r_y_low_limit = 0
r_y_high_limit = 0
r_z_low_limit = 0
r_z_high_limit = 0

def move_SMU_PGM2():
    yield from mv(user_ns[user_ns_smu_name].t_x, 0,
          user_ns[user_ns_smu_name].r_x, 0,
          user_ns[user_ns_smu_name].r_y, 0,
          user_ns[user_ns_smu_name].r_z, 0)

 

def move_SMU_PGM2_user(_tx, _rx, _ry, _rz):
    if (t_x_low_limit <= _tx <= t_x_high_limit) and (r_x_low_limit <= _rx <= r_x_high_limit) \
       (r_y_low_limit <= _ry <= r_y_high_limit) and (r_z_low_limit <= _rz <= r_z_high_limit):
        yield from mv(user_ns[user_ns_smu_name].t_x, _tx,
            user_ns[user_ns_smu_name].r_x, _rx,
            user_ns[user_ns_smu_name].r_y, _ry,
            user_ns[user_ns_smu_name].r_z, _rz)
    elif (t_x_low_limit <= _tx <= t_x_high_limit) == False:
        print('The used parameter for the tx movement is out of range! Limits are: {t_x_low_limit} to {t_x_high_limit}.') 
        return(yield from null())
        #return null and message 
    elif (r_x_low_limit <= _rx <= r_x_high_limit) == False:
        print('The used parameter for the rx movement is out of range! Limits are: {r_x_low_limit} to {r_x_high_limit}.') 
        return(yield from null())
    elif (r_y_low_limit <= _ry <= r_y_high_limit) == False:
        print('The used parameter for the ry movement is out of range! Limits are: {r_y_low_limit} to {r_y_high_limit}.') 
        return(yield from null())
    elif (r_z_low_limit <= _rz <= r_z_high_limit) == False:
        print('The used parameter for the rz movement is out of range! Limits are: {r_z_low_limit} to {r_z_high_limit}.') 
        return(yield from null())
    
    
"""
def move_SMU_PGM2_b():
    #print("I move M3-mirror to SISSY-position")
#    yield from mv(user_ns["u17_m3"].start_immediately, 1)
    yield from mv(user_ns["u17_m3"].choice, '???',
          user_ns["u17_m3"].t_x, 4140,
          user_ns["u17_m3"].r_x, 0,
          user_ns["u17_m3"].r_y, -160,
          user_ns["u17_m3"].r_z, 350)
#    yield from mv(user_ns["u17_m3"].start_immediately, 0) 
#    yield from mv( user_ns["u17_m3"].r_z, -4000)
"""
############################################################################
# SLITS
"""
def move_slitS():
    print("\nI move exit slit SISSY to standard opening (100 um)")
    print("\nHorizontal apertures should be at position 4/10 mm (L/R)")
    yield from mv(user_ns["u17_es_sissy"].setpoint, 100) 

def move_slitS_out():
    print("\nI open exit slit SISSY (2600 um)")
    print("\nHorizontal apertures should be at position 4/10 mm (L/R)")
    yield from mv(user_ns["u17_es_sissy"].setpoint, 2600) 
"""