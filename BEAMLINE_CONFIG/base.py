from bluesky import RunEngine
RE = RunEngine({})


from bluesky.callbacks.best_effort import BestEffortCallback
bec = BestEffortCallback()


# Send all metadata/data captured to the BestEffortCallback.
RE.subscribe(bec)


# Get the databroker https://gist.github.com/untzag/53281819709b1058c7708a250cbd3676
import databroker
db = databroker.catalog["aquarius"]

# Insert all metadata/data captured into db.
RE.subscribe(db.v1.insert)


# If you need debug messages from the RE then uncomment this
#from bluesky.utils import ts_msg_hook
#RE.msg_hook = ts_msg_hook


# Configure persistence between sessions of metadata
from bluesky.utils import PersistentDict
import os
cwd = os.getcwd()
RE.md = PersistentDict('~/bluesky/persistence/beamline')


import databroker.core
handler_registry = databroker.core.discover_handlers()


# Import matplotlib
import matplotlib.pyplot as plt
from bluesky.callbacks.zmq import Publisher 
publisher = Publisher("localhost:5577") 
RE.subscribe(publisher)

