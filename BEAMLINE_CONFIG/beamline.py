from bessyii_devices.ring import Ring
from bessyii_devices.keithley import Keithley6514, Keithley6517
from bessyii_devices.pgm import PGM, PGM_Aquarius
from bessyii_devices.undulator import UndulatorUE56_2
from bessyii_devices.au import AU1Aquarius
from bessyii_devices.pinhole import Pinhole2
from bessyii_devices.m3_m4_mirrors import SMUAquariusPGM1, SMUAquariusPGM2
from bessyii_devices.keithley_manipulate_signal import ManipulateKeithleysSignals
#from bessyii_devices.lock import Lock


try:
    from bluesky.magics import BlueskyMagics
    get_ipython().register_magics(BlueskyMagics)

    from beamlinetools.plans.magics_aquarius import BlueskyMagicsAQUARIUS    
    get_ipython().register_magics(BlueskyMagicsAQUARIUS)
    
except NameError as e:
    print('Warning: ', e)


# simulated devices
from ophyd.sim import det1, det2, det3, det4, motor1, motor2, motor, det   # two simulated detectors

#Beamline Lock
#lock = Lock("AQUARIUSEL:LOCK00:BEAMLINE:", name="lock")


# Ring
bessy2 = Ring('MDIZ3T5G:', name='ring')


# Keithleys
kth01 = Keithley6517('AQUARIUSEL:' + 'Keithley01:', name='Keithley01', read_attrs=['readback'])
kth02 = Keithley6514('AQUARIUSEL:' + 'Keithley02:', name='Keithley02', read_attrs=['readback'])


# Manipulate Keithley signals
# I comment this out because it creates problems
#kth_ratio = ManipulateKeithleysSignals('AQUARIUSEL:',  name='Keithley')


# Pgm
pgm = PGM_Aquarius('pgm2os15l:', name='pgm')


# Undulators U49ID8R
# Changed to UE56ID8R
u56_2 = UndulatorUE56_2('UE56ID8R:', name='u56_2') #, read_attrs=['gap_rb', 'harmonic_01_eV'])


# Apertures
au = AU1Aquarius('AUYU15L:', name = 'au')


# Pinhole
ph = Pinhole2('PHYU15L:', name = 'ph')


# Mirrors
#u49_2_smu_pgm2 = SMU_PGM1('SMUYU115L:' ,name='u49_2_smu_pgm1')
m1 = SMUAquariusPGM2('SMUYU115L:', name='m1')
