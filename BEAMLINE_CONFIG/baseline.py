from bluesky.preprocessors import SupplementalData
from .base import *
from .beamline import *

sd = SupplementalData()
RE.preprocessors.append(sd)

#sd.baseline = [u49_2.gap, u49_2.harmonic_01_eV, u49_2.harmonic_01_nM, 
#               au.top.readback, au.bottom.readback, au.right.readback, au.left.readback,
#               m1.tx.readback, m1.rx.readback, m1.ry.readback, m1.rz.readback, 
#               pgm.en, pgm.cff, pgm.diff_order, pgm.alpha, pgm.beta, pgm.theta]