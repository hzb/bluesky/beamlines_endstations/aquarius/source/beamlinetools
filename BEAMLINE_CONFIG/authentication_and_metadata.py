#from bessyii.eLog import requestInvestigationName,getInvestigationIDFromTitle,getInvestigationIDFromName,getSessionID, writeToELog
from bessyii.eLog import requestInvestigationName, getSessionID, writeToELog, ELogCallback, authenticate_session, logout_session
from bessyii.locks import teardown_my_shell, lock_resource
from getpass import getpass
import Levenshtein as lev
import requests
import logging
import json
from datetime import datetime, timedelta
from bluesky.callbacks import CallbackBase
from pprint import pformat
import atexit
from .base import *
from .beamline import *

import socket
RE.md['hostname'] = socket.gethostname()



#######   ---- eLog ----------------------


from jinja2 import Template
start_template ="""
<b style="color:#333;"> Plan Started by {{user_name}}</b>
<br>{{- plan_name }} ['{{ uid[:6] }}'] (scan num: {{ scan_id }})
<br> command: {{ command_elog }}
<br>---------
{% if  operator is defined %}
    <br>operator :    {{operator}}
{% endif %}
{% if  reason is defined %}
    <br>reason :    {{reason}}
{% endif %}
{% if  comment is defined %}
    <br>comment :    {{comment}}
{% endif %}
<hr style="height:2px;border:none;color:#333;background-color:#333;" />"""



j2_start_template = Template(start_template)


end_template ="""
<hr style="height:2px;border:none;color:#333;background-color:#333;" />
<b>Plan ended</b>
<br>exit_status: {{exit_status}}
<br>num_events: {{num_events}}
<br>uid:{{run_start}}
<br>
"""


j2_end_template = Template(end_template)

beamline_status_template ="""
<b>Beamline Status</b>
<br>u49_2_gap :          {{u49_2}}
<br>u49_2_harmonic (eV): {{'%04d' %u49_2_harmonic_01_eV}} eV
<br>u49_2_harmonic (nM): {{'%04d' %u49_2_harmonic_01_nM}} nM
<br>au_top :      {{au_top}}
<br>au_bottom :   {{au_bottom}}
<br>au_left :     {{au_left}}
<br>au_right :    {{au_right}}
<br>pgm :    {{pgm_en}}
<br>pgm.cff :    {{pgm_cff}}
<br>pgm.diff_order :    {{pgm_diff_order}}
<br>pgm.alpha :    {{pgm_alpha}}
<br>pgm.beta :    {{pgm_beta}}
<br>pgm.theta :    {{pgm_theta}}
<br>m1.tx :    {{m1_tx}}
<br>m1.rx :    {{m1_rx}}
<br>m1.ry :    {{m1_ry}}
<br>m1.rz :    {{m1_rz}}
"""

j2_baseline_template = Template(beamline_status_template)

#Se up our locks and environment
#####
#lock_list = [lock]

#lock_list =[None]

#def logout():
#    logout_session(RE, lock_list)

#def authenticate():
#    authenticate_session(RE, db, lock_list)
    
## Connect the shell teardown on exit
#atexit.register(teardown_my_shell, RE, lock_list)  
#####

## Find out what the user wants to do
choice = input(f"\nConnect to eLog? Y/N:")


#Clear the metadata
if 'investigation_title' in RE.md:
    del RE.md['investigation_title']

if 'investigation_id' in RE.md:
    del RE.md['investigation_id']

if 'eLog_id_num' in RE.md:
    del RE.md['eLog_id_num']

if 'user_profile' in RE.md:
    del RE.md['user_profile']

if 'user_name' in RE.md:
    del RE.md['user_name']

if choice == 'Y' or choice == 'y':

    authenticate()
    

else: 
    
    print("You can always authenticate later with \'authenticate()\'")
#    lock_resource(RE, lock_list)

#Subscribe the callback
RE.subscribe(ELogCallback(db, j2_start_template, j2_baseline_template, j2_end_template))



