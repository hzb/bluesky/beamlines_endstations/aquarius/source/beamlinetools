## Date (Day-Month-Year)
- 05-03-2024 
  

## IPython Profile control of AQUARIUS beamline U56/2

Three packages are used

| package | description |
| ------ | ------ |
|[bessyii version: 5c653a4ff7048c6e351b65f577865edf9370b7f5](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii/-/tree/v1.3.0?ref_type=tags) | contains shared scripts, plans and tools for the whole facility |
|[bessyii_devices version: 80c47863284627f9a84a96c21fca4d0c78323f1a](https://codebase.helmholtz.cloud/hzb/bluesky/core/source/bessyii_devices/-/tree/80c47863284627f9a84a96c21fca4d0c78323f1a) | contains ophyd devices for bessyii |


## Release v0.1.0
FIXME: This is the docker image release version of 2023.

Included devices:
- m1
- pgm (with slit)
- au 
- ph
- Keithleys (kth1, kth2)
- ue56_2


## Changes (history)
- ...
- 15-02-2023
- 24-02-2023 (latest)
- 05-03-2024 (new architecture)
